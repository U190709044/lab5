import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

    public static void main(String[] args) throws FileNotFoundException{
        int matrix[][] = readMatrix();
        String numbers[]= new String[10];
        numbers[9]=" ";
        boolean found = false;
        found: for (int i=0; i< matrix.length; i++) {
            for (int j=0; j < matrix[i].length; j++) {
                if (search(0,matrix, i,j,numbers)){
                    found = true;
                    break found;
                }
            }
        }

        if (found) {
            System.out.println("A sequence is found");
        }


        for(int i=0;i<10;i++){
            int row= numbers[i].charAt(0);
            int roww = Character.getNumericValue(row);
            int col= numbers[i].charAt(2);
            int coll = Character.getNumericValue(col);
            matrix[roww][coll]=9-matrix[roww][coll];

        }

        printMatrix(matrix);
    }

    private static boolean search (int number, int[][]matrix, int row, int col,String[] numbers) {

        if(number==9 && matrix[row][col]==9){
            if(numbers[9]==" ")
                numbers[number]= row+" "+col+" "+number;


            return true;


        }
        else if(matrix[row][col]==number) {
            if(numbers[9]==" ")
                numbers[number]= row+" "+col+" "+number;
            if (row != 0)
                search(number + 1, matrix, row - 1, col,numbers);
            if (col != 9)
                search(number + 1, matrix, row, col + 1,numbers);
            if (row != 9)
                search(number + 1, matrix, row + 1, col,numbers);
            if (col != 0)
                search(number + 1, matrix, row, col - 1,numbers);





        }

        return false;

    }

    private static int[][] readMatrix() throws FileNotFoundException{
        int[][] matrix = new int[10][10];
        File file = new File("matrix.txt");

        try (Scanner sc = new Scanner(file)){


            int i = 0;
            int j = 0;
            while (sc.hasNextLine()) {
                int number = sc.nextInt();
                matrix[i][j] = number;
                if (j == 9)
                    i++;
                j = (j + 1) % 10;
                if (i == 10)
                    break;
            }
        } catch (FileNotFoundException e) {
            throw e;
        }
        return matrix;
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
    }
}
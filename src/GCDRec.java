public class GCDRec {
    public static void main(String[] args) {
        int a,b,gcd;


        a=Integer.parseInt(args[0]);
        b=Integer.parseInt(args[1]);
        System.out.println(GCD(a, b));
    }

    static int GCD(int c, int d){
        if (d == 0){
            return c;
        }
        int r = c % d;
        if (r == 0) {
            return d;
        }
        return GCD(d, r);
    }
}

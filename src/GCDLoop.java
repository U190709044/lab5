

public class GCDLoop {
    public static void main(String[] args) {
        int a,b;
        int max, min, r;

        a=Integer.parseInt(args[0]);
        b=Integer.parseInt(args[1]);
        if (a > b) {
            max = a;
            min = b;
        } else {
            max = b;
            min = a;
        }
        while (min != 0) {
            r = max % min;
            max = min;
            min = r;
        }
        System.out.println(max);
    }
    }

